<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Backend extends CI_Controller
{
     
  public function __construct()
    {
         parent::__construct();
         $js_folder=$this->i18n->get_current_locale(); 
         $this->lang->load('messages', $js_folder);
    }


    
    function admin() 
    { 
      $js_folder=$this->i18n->get_current_locale(); 
      
      $this->lang->load('messages', $js_folder);
      $successmsg =$this->lang->line('default_success_msg');

      // debug($successmsg);
      // die;
 

      $this->load->model('Mui'); 	 
      
      $page=$this->Mui->getCommPage('backend', $js_folder);
      
      $user = $this->session->userdata('user');
      $roles   =$this->session->userdata('roles');
      $role_list=array_retrieve($roles ,'role_code');
      $isadmin=false;
      if( in_array( 'admin' ,$role_list  )){$isadmin=true;}
      
      if(!$isadmin)
      {
      echo "Not admin,can not access backend .";
      return;
      }
      
      if (empty($user)) 
      {
 	    $page['loginview']='loginview';
	    $this->load->view('framework',$page );
      }
      else
      {
	    $this->load->view('backend_view',$page );
      }
    }
    
    
    
    function url()
    {
      $file=$_GET['file'];
      $imgstr=img($file);
      echo  "<div style='margin:20px;'>".$imgstr.'<br/>'.$file."</div>";
    }
 }
?>
