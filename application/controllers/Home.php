<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Home extends CI_Controller
{
 



    function check_app_dirs_writeable(){
            
            $default_upload_folder=  FCPATH.$this->config->item('default_upload_folder');
            $default_js_upload_folder= FCPATH.$this->config->item('default_js_upload_folder');
            $default_tmp_folder=  FCPATH.$this->config->item('default_tmp_folder');
            $logfile=   helper_getlogname();    
            $logdir=FCPATH.$this->config->item('logdir'); 

            $this->load->model('Mfile');
             
            
            if( ! $this->Mfile->checkWriteAble($default_upload_folder) ){
                  echo "$default_upload_folder not exists or not writeable";
                  die;
            }


            if( ! $this->Mfile->checkWriteAble($default_js_upload_folder) ){
                  echo "$default_js_upload_folder not exists or not writeable";
                  die;
            }
             
            if( ! $this->Mfile->checkWriteAble($default_tmp_folder) ){
                  echo "$default_tmp_folder not exists or not writeable";
                  die;
            }

            if( ! $this->Mfile->checkWriteAble($logdir) ){
                  echo "$logdir not exists or not writeable";
                  die;
            }

             
            if (  $this->config->item('uselog')){
                 @$log   = fopen($logfile, 'a+');
                 if(! $log){
                  echo "$logfile not exists or not writeable";
                  die;
                 }
            } 
    }
 

    function index()
    {
        
       
       
        $this->check_app_dirs_writeable();
        
        $this->refresh_session();
        $lang = $this->i18n->get_current_locale();
        $this->load->model('Mui');
        $page          = $this->Mui->getCommPage('front', $lang);
        $activities    = $this->session->userdata('user_activity');
        $left          = $this->Mui->getActionBlock($activities);
        $page['left']  = $left;
        $page['right'] = 'jpanel_right';


        // debug($page);
        // die;

        $this->load->view('framework', $page);
        
    }


    function refresh_session(){
         
         $ci_sess= $this->session->all_userdata();



         if(  isset($ci_sess['eid'])  &&    isset($ci_sess['user'])   ){
             
             $this->setsession($ci_sess['eid'], $ci_sess['user']);
         }
    }
    
    
    function checkDatabaseConnection()
    {
        if ($this->load->database() === FALSE) {
            exit('Database not setted.');
        } else {
            return true;
        }
    }
    
    function login()
    {

        if ($this->uri->segment(3) === FALSE) {
            $lang = $this->i18n->get_current_locale();
            $this->i18n->load_language();
        } else {
            $lang = $this->uri->segment(3);
            $this->i18n->set_current_locale($lang);
        }
        
        $this->load->model('Mui');
        $page              = $this->Mui->getCommPage("login", $lang);
        $page['lang']      = $lang;
        

        $page['loginview'] = 'loginview';
        $this->load->view('framework', $page);
         
    }
    
    
    function dologin()
    {
        
     
        $post = file_get_contents('php://input');
        $ppp_post= $_POST;

        $p    = (array) json_decode($post);


        $u    = array(
            'user' => $p['account']
        );

        $result = array();
        
        $user = $this->db->select('pid,user,password,staff_name,active,salt')->get_where('nanx_user', $u)->result_array();
        
        
        if (sizeof($user) != 1) {
            $result['code']  = -1;
            $result['msg']   = $this->lang->line('login_err');
            $result['einfo'] = 'auth failed';
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
            return;
        }
        
        
        if ($user[0]['active'] != 'Y') {
            $result['code'] = -2;
            $result['msg']  = $this->lang->line('user_locked');
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
            return;
        }
        
        
        if (sizeof($user) == 1) {
            $salt              = $user[0]['salt'];
            $pwd_db            = $user[0]['password'];
            $pwd_try           = $p['password'];
            $pwd_try_with_salt = md5(md5($pwd_try) . $salt);
            
            
            if ($pwd_try_with_salt == $pwd_db) {
                $result['code'] = 0;
                $result['msg']  = '';
                $result['user'] = $p['account'];
                $this->load->library('session');
                $eid = $this->config->item('eidfolder');
 
                
                
                $this->load->library('session');

                $this->session->set_userdata('eid', $eid);
                $this->session->set_userdata('user',  $p['account']);
 
                $this->setsession( $eid, $p['account']);

                $sess_array=array('eid'=>$eid,'user'=> $p['account'],'datasrc'=>'dologin');
                $this->session->set_userdata($sess_array);
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                
 
            } else {
                $result['code']  = -1;
                $result['msg']   = $this->lang->line('login_err');
                $result['einfo'] = 'auth failed';
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                return;
            }
        }
    }
    
    
    
 function setsession($eid,$user)
    {   

 
        $session_data=array();
        $this->load->model('Muserrole');
        $this->load->model('Msystempara');
        
        $session_data['eid']=$eid;
        $session_data['user']=$user;
           
        $sql   = "select role_code from nanx_user_role_assign where user='" . $user . "' ";
        $roles = $this->db->query($sql)->result_array();
        $session_data['roles']=$roles;
        
        
        $activity_list = $this->Muserrole->getActivitybyRoleCode($roles);
        $activity      = array_retrieve($activity_list, 'activity_code');
        $session_data['user_activity']=$activity;
        
        $sql      = "select  *  from nanx_user where user='" . $user . "'";
        


        $user_get = $this->db->query($sql)->row_array();
        $session_data['staff_name']=$user_get['staff_name'];
 
        $sql        = "select  *  from nanx_who_is_who where user='" . $user . "'";
        $who_is_who = $this->db->query($sql)->result_array();
        $session_data['who_is_who']=$who_is_who;
        
        $page_title   = $this->Msystempara->getCfgItem('PAGE_TITLE');
        $banner_title = $this->Msystempara->getCfgItem('BANNER_TITLE');
      
        $session_data['page_title']=$page_title;
        $session_data['banner_title']=$banner_title;
        $this->session->set_userdata($session_data);
 

    }
    
    


    function logout()
    {
      
       $this->session->sess_destroy();
       redirect('home/login');
    }
    
    function test()
    {
        $this->load->library('email');
        $this->email->from('tangyongjin97@qq.com', 'Tang');
        $this->email->to('tangyongjin97@qq.com');
        $this->email->subject('Email Test');
        $this->email->message('测试邮件,中文');
        $this->email->send();
        echo $this->email->print_debugger();
    }
}
?>
