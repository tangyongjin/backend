<?php

 
 
 function sysdatetime()
 {
 	date_default_timezone_set("Asia/Chongqing");
  $cur= date("Y-m-d H:i:s", time()); 
  return $cur;
 }
 
 function sysdate()
 {
 	date_default_timezone_set("Asia/Chongqing");
  $cur= date("Y-m-d", time()); 
  return $cur;
 }
 
 function sysdate_format($format)
 {
 	date_default_timezone_set("Asia/Chongqing");
  $cur= date($format, time()); 
  return $cur;
 }


 function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

 
?>